module descomposicionLU
    use nrtype

!
!Definicion de variables a definirse por el modulo
!
    implicit none

!    
!Fin definicion de variables
!



!
!Definicion de subrutinas y funciones
!
contains

subroutine LUd(A, LU, bk)
    implicit none

    real(DP), intent (in), dimension(:,:)                 :: A
    integer(I2B)                                          :: N
    real(DP), intent (out), dimension(:,:)                :: LU
    real(DP), dimension(size(A,1))                        :: raux
    integer(I2B), dimension(size(A,1),2)                  :: bk
    integer(I2B)                                          :: i, j, k, pmax, bki
     
    character(8)                                          :: formatofila='(  F7.2)'

    N=size(A,1)
    bki=0
    bk=-1
    
    write(formatofila(2:3),'(I2)') N

!    print '("---------------------", /, "dentro de la rutina", /, "---------------------")'

    !allocate(LU(N,N))

    externo: do j=1,N

        interno1: do i=1,j
                forall (k=1:i)
                     LU(i,j)=A(i,j)-sum(LU(i,1:k)*LU(1:k,j))
                endforall
                
!                print *, '--| i=',i,' j=',j,'|-- INTERNO1'
!                print formatofila, LU
        enddo interno1

        interno2: do i=j+1,N
!                    if(j.eq.2)  LU(j,j)=0

!                print *, LU(j,j)
                
                if(LU(j,j).eq.0.0) then
                    raux=maxloc(LU((j+1):N,j)) ! Me devuelve un vector de dos dim, por eso uso raux
                    pmax=raux(1)

!                    print *, 'antes del pivoteo: '
!                    print formatofila, LU

                    raux(:)=LU(i,:)
                    LU(i,:)=LU(pmax,:)
                    LU(pmax,:)=raux(:)


                    bki=bki+1
                    bk(bki,:)=(/i,pmax/)
                    
!                    print *, 'despues del pivoteo'
!                    print formatofila, LU
!                    print *, bk(:,1)
!                    print *, bk(:,2)
                 endif
                
                forall (k=1:j)
                    LU(i,j)=(1/LU(j,j))*(A(i,j)-sum(LU(i,1:k)*LU(1:k,j)))
                endforall
                
!                print *, '--| i=',i,' j=',j,'|-- INTERNO2'
!                print formatofila, LU

        enddo interno2

    enddo externo


!    print '("---------------------", /, "sale de la rutina", /, "---------------------")'
endsubroutine LUd



subroutine ReorgMat(MA, bk)

    implicit none
    real(DP), dimension(:,:), intent(inout)   :: MA
    integer(I2B), dimension(:,:)              :: bk
    integer(I2B)                              :: bki, N
    real(DP), dimension(size(MA,1))           :: raux

    N=size(bk,1)

    do  bki=1,N
        if((bk(bki,1).EQ.(-1)).OR.(bk(bki,2).EQ.(-1))) then
             exit
        else
            raux=MA(bk(bki,1),:)
            MA(bk(bki,1),:)=MA(bk(bki,2),:)
            MA(bk(bki,2),:)=raux
        endif        
    enddo

endsubroutine ReorgMat

endmodule descomposicionLU
