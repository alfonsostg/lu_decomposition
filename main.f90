    program main

    use descomposicionLU
    use FrontBack

    implicit none

    integer, parameter                      :: N=5, Nb=1
    integer                                 :: i,j
    real(DP), dimension(N,N)                :: MA, MLU
    integer(I2B), dimension(size(MA,1),2)   :: Vbk
    real(DP), dimension(N,Nb)               :: b, x

    character(8)                            :: formatofila='(  F7.2)'
  



    write(formatofila(2:3),'(I2)') N

    forall(i=1:size(MA,1),j=1:size(MA,2)) 
        MA(i,j)=real((0.7*real(i))**j)*0.1
        b(i,1)=sqrt(real(i))
    endforall

!    MA(1,:)=(/1.0,   7.0,     11.0/)
!    MA(2,:)=(/14.0,  24.0,    19.0/)
!    MA(3,:)=(/7.0,   8.0,     9.0/)

!    b(:,1)=(/3.0, 1.0, 2.5/)
!   b(1,:)=(/1.0, 0.0, 0.0/)
!   b(2,:)=(/0.0, 1.0, 0.0/)
!   b(3,:)=(/0.0, 0.0, 1.0/)
   
    print *, 'matriz original: '
    write(6, formatofila) MA

    print *, 'matriz b:'
    print *, b

    print '("|-------------Se esta buscando la solucion...-----------|")'

    call LUd(MA, MLU, Vbk)
    

    x=FrontSLU(MLU,b)

!   print *, 'resultado frontSLU', x

    x=BackS(MLU,x)

    call ReorgMat(MA, Vbk)
    call ReorgMat(b, Vbk)
    call ReorgMat(x, Vbk)


    print '("|-------------Solucion hallada!.-----------|")'

    write(6, *), '|--descomposicion LU--|'

    write(6, formatofila) MLU


    print *, 'Matriz original modificada: '
    write(6, formatofila) MA
!   print *, b
    print *, 'Resultado:'
    print *, x
 

endprogram main
