module FrontBack

    use nrtype

    implicit none

contains

function FrontSLU(A,B) result (X)
    implicit none
    real(DP), dimension(:,:), intent (in)       :: A, B
    real(DP), dimension(size(B,1),size(B,2))    :: X
    integer(I2B)                                :: i, j, NA, NB

    NA=size(A,1)
    NB=size(B,2)

    X=0.0


!   print '("---------------------", /, "dentro de la rutina", /, "---------------------")'


!   print *, 'vector X(i,j), tamanio i:', NA, ' tamanio j:', NB

!   do i=1,NA
!       print *, A(i,:)
!   enddo


    do j=1,NB
        do i=1,NA
            X(i,j)=B(i,j)-sum(A(i,1:i-1)*X(1:i-1,j))
            
!           print *, B(i,j)
!           print *, sum(A(i,1:i-1)*X(1:i-1,j))

!           print *, 'i=',i, ' j=', j
!           print *, X
        enddo
    enddo


!   print '("---------------------", /, "fuera de la rutina", /, "---------------------")'

endfunction FrontSLU


function BackS(A,B) result(X)
    implicit none
    real(DP), dimension(:,:), intent (in)       :: A, B
    real(DP), dimension(size(B,1),size(B,2))    :: X
    integer(I2B)                                :: i, j, NA, NB

    NA=size(A,1)
    NB=size(B,2)

    do j=1,NB
        do i=NA,1,-1
            X(i,j)=(B(i,j)-sum(A(i,i+1:nA)*X(i+1:nA,j)))/A(i,i)
        enddo
    enddo
    
endfunction BackS

!function FrontS(A,B) result(X)
!    implicit none
!    real(DP), dimension(:,:), intent (in)       :: A, B
!    real(DP), dimension(size(B,1),size(B,2))    :: X

!    X=Backs(A(3:1,:),B)

!endfunction FrontS

endmodule FrontBack
